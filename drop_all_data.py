#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import config as cfg

from influxdb import InfluxDBClient

db = InfluxDBClient(**cfg.influx_config)
data = db.query("drop series from /./")
