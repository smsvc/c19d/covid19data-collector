#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

# pylint: disable=missing-module-docstring,wrong-import-position

import sys
import logging
from logging.config import dictConfig

import config as cfg

from influxdb import InfluxDBClient

sys.path.insert(0, "..")

import get_references as ref
import import_cases as cases
import import_icu as icu

dictConfig(cfg.logging_config)
log = logging.getLogger()

db = InfluxDBClient(**cfg.influx_config)

# db.query("drop series from /./")  # drop all entries

entries = db.query("select * from de")
if len(entries) != 0:
    log.info("database not empty - aborting import")
    sys.exit()

# create references
ref.get_all()

# import history
cases.import_all()
icu.import_all()
