update_time = "05:00"

influx_config = dict(
    host="influxdb",
    port="8086",
    database="c19d",
    username="influx",
    password="CorrectHorseBatteryStaple",
)

logging_config = dict(
    version=1,
    formatters={
        "f": {
            "format": "%(asctime)s %(filename)s %(levelname)s %(message)s",
            "datefmt": "%Y-%m-%dT%H:%M:%S",
        }
    },
    root={
        "handlers": ["h"],
        "level": "INFO",
    },
    handlers={
        "h": {
            "class": "logging.StreamHandler",
            "formatter": "f",
        }
    },
)
