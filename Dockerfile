FROM python

COPY . /collector

WORKDIR /collector

RUN apt-get update && apt-get install -y tini
RUN pip install -r requirements.txt

ENTRYPOINT ["tini", "-e 130", "--"]
CMD ["python", "scheduler.py"]
